import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
  path: 'hello',
  loadChildren: () => import('./modules/hello/hello.module').then(m => m.HelloModule)
},
{
  path: 'info',
  loadChildren: () => import('./modules/info/info.module').then(m => m.InfoModule)
},
{
   path: '**', redirectTo: '/hello', pathMatch: 'full'  
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
