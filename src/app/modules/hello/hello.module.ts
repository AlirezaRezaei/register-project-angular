import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloComponent } from './components/hello/hello.component';
import { HelloRouting } from './hello-routing.module';

@NgModule({
  declarations: [HelloComponent],
  imports: [CommonModule, HelloRouting],
})
export class HelloModule {}
