import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoListComponent } from './components/info-list/info-list.component';

 
const routes: Routes = [
    { path: '', component: InfoListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfoRoutingModule { }
