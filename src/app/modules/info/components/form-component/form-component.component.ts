import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  SimpleChanges,
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as uuid from 'uuid';

@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.scss'],
})
export class FormComponentComponent implements OnInit {
  constructor() {}
  public registerForm: FormGroup;

  @Output() user = new EventEmitter<any>();
  public disableButton: boolean = false;
  ngOnInit(): void {
    this.initForm();
  }

  ageChange(event) {
    let age = parseInt(event);
    if (isNaN(age)) {
      this.disableButton = true;
    } else {
      this.disableButton = false;
    }
  }

  initForm() {
    this.registerForm = new FormGroup({
      name: new FormControl(''),
      age: new FormControl(''),
    });
  }

  onSubmit() {
    let data: object = {
      id: uuid.v4(),
      name: this.registerForm.get('name').value,
      age: this.registerForm.get('age').value,
    };
    this.user.emit(data);
  }
}
