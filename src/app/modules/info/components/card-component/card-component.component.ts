import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card-component',
  templateUrl: './card-component.component.html',
  styleUrls: ['./card-component.component.scss'],
})
export class CardComponentComponent implements OnInit {
  constructor() {}

  @Input() cardDetail: object;
  @Output() userCard = new EventEmitter<any>();
  
  ngOnInit(): void {}

  remove(id:string) {
    this.userCard.emit(id);
  }
}
