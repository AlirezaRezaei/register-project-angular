import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-list',
  templateUrl: './info-list.component.html',
  styleUrls: ['./info-list.component.scss']
})
export class InfoListComponent {

  public userList: any[] = [];
  public userExist:string;
  public isUserExist:Boolean=false

  public removeUserCard(id: string) {
    for (let item in this.userList) {
      if (this.userList[item].id === id) {
        this.userList.splice(parseInt(item), 1);
      }
    }
  }

  public user(data: any): void {
    this.isUserExist = false
    if (data) {
      let userExist = this.userList.find(
        (item)=>{
          if (item.name === data.name){
           return item
          }
        }
      )

      if(userExist){
        this.isUserExist = true
        this.userExist = userExist.name
        return
      }
      this.userList.push(data);
      
    }
  }

}
