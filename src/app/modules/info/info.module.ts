import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import{CardComponentComponent} from './components/card-component/card-component.component';
import{FormComponentComponent} from './components/form-component/form-component.component';
import{InfoListComponent} from './components/info-list/info-list.component';
import {InfoRoutingModule} from './info-routing.module';


@NgModule({
  declarations: [CardComponentComponent,FormComponentComponent,InfoListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InfoRoutingModule
  ]
})
export class InfoModule { }
